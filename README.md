## Belajar Coding

## Introduction

Belajar coding displays the entire posts with 10 posts per page pagination, showing the number of total comments each of post and one-or two-click access to detail post.

## Requirements

This project requires `npm i react-router-dom` package contains bindings for using React Router in web applications.

## Login Requirements

Dilakukan pengecekan dari service diatas apakah username tersebut ada atau tidak. 

![login-page](./login-page.png)

Jika tidak ada maka dimunculkan alert 'Wrong password'

![wrong-password](./login-page-wrong.png)

## Dashboard Page 

Pagination dengan minimal 10 data yang tampil,  dengan next dan previous page.

![dashboard](./dashboard.png)

![paging](./paging.png)

## Detail Page

Menampilkan detail page dan semua comment yang didapat dari fetch data comment berdasarkan by id post.

![detail](./detail.png)

![detail-comment](./detail-comment.png)

## Detail Profile

Menampilkan data profile yang didapat setelah login yang sudah disimpan di local storage.

![detail-profile](./detail-profile.png)

## Credits

### Tools used:

- [React.js](https://reactjs.org/)
- [MUI Core](https://mui.com/material-ui/material-icons/)





