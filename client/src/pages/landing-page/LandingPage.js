import React from 'react';
import './landing-page.css';
import bckgrndimg from './5395379.jpg';
import Header from '../../components/header/Header';

export default function Home() {
  return (
    <div className='container'>
        <Header page={'landing-page'}/>
        <div className='hero'>
            <img src={bckgrndimg} alt='image-landing-page' className='hero-image'/>
        </div>
    </div>
  )
}
