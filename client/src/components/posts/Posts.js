import React, { useCallback, useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import ReactLoading from 'react-loading';

export default function Posts() {
  const [posts, setPosts] = useState([]);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  let navigate = useNavigate();

  const getPosts = useCallback(async() => {
    const response = await fetch("https://jsonplaceholder.typicode.com/posts");
    const json = await response.json();
    
    setPosts(json);
  });

  useEffect(() => {
    getPosts();
  }, []);

  useEffect(() => {
    async function fetchCommentCounts() {
      const promises = posts.map(async post => {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}/comments`);
        const json = await response.json();
        return {
          id: post.id,
          commentCount: json.length,
        };
      });

      const commentCounts = await Promise.all(promises);
      const updatedPosts = posts.map(post => {
        const count = commentCounts.find(count => count.id === post.id);
        return {
          ...post,
          commentCount: count ? count.commentCount : 0,
        };
      });
      setPosts(updatedPosts);
      setLoading(false);
    }

    if (posts.length > 0) {
      fetchCommentCounts();
    }
  }, [posts]);

  const selectPageHandler = useCallback((selectedPage) => {
    if (selectedPage >= 1 && selectedPage <= posts.length / 10 && selectedPage !== page) {
      setPage(selectedPage)
    }
  });

  const detailPost = useCallback((id) => {
    navigate(`/status/${id}`, { replace: true })
  });
  return (
    <div className='content'>
          <div className='search-bar'>
            <div className='searcy-input-label'>
              <div dir="ltr" className="input-search">
                <input aria-activedescendant="typeaheadFocus-0.3325319651391889" aria-autocomplete="list" aria-label="Search query" aria-owns="typeaheadDropdown-2" autoCapitalize="sentences" autoComplete="off" autoCorrect="off" placeholder="Search" role="combobox" spellCheck="false" enterKeyHint="search" type="text" dir="auto" className=""/>
              </div>
              <div className="container-icon">
                <svg viewBox="0 0 24 24" aria-hidden="true" className="icon-search"><g><path d="M10.25 3.75c-3.59 0-6.5 2.91-6.5 6.5s2.91 6.5 6.5 6.5c1.795 0 3.419-.726 4.596-1.904 1.178-1.177 1.904-2.801 1.904-4.596 0-3.59-2.91-6.5-6.5-6.5zm-8.5 6.5c0-4.694 3.806-8.5 8.5-8.5s8.5 3.806 8.5 8.5c0 1.986-.682 3.815-1.824 5.262l4.781 4.781-1.414 1.414-4.781-4.781c-1.447 1.142-3.276 1.824-5.262 1.824-4.694 0-8.5-3.806-8.5-8.5z"></path></g></svg>
              </div>
            </div>
          </div>

          { loading && <div className='react-loading'><ReactLoading color='#5b94ed' type={'spin'} height={35} width={35} /></div> }

          { !loading && posts?.slice(page * 10 - 10, page * 10).map((data, id) => {
            return (
              <div className='posts' key={id}>
                <div className='satu'>Abit</div>
                <div className='dua'>
                    <div className='text-post'>{data.title}</div>
                    <div className='comments-detail'>
                      <div className='comments-detail-container'>
                        <div className='comments-container'>
                        <div className='icon-comment-container'>
                            <svg viewBox="0 0 24 24" aria-hidden="true" className="icon-comment"><g><path d="M1.751 10c0-4.42 3.584-8 8.005-8h4.366c4.49 0 8.129 3.64 8.129 8.13 0 2.96-1.607 5.68-4.196 7.11l-8.054 4.46v-3.69h-.067c-4.49.1-8.183-3.51-8.183-8.01zm8.005-6c-3.317 0-6.005 2.69-6.005 6 0 3.37 2.77 6.08 6.138 6.01l.351-.01h1.761v2.3l5.087-2.81c1.951-1.08 3.163-3.13 3.163-5.36 0-3.39-2.744-6.13-6.129-6.13H9.756z"></path></g></svg>
                        </div>
                          <div className='count-comments'>
                            <span>{data.commentCount}</span>
                          </div>
                        </div>
                        <div className='detail-btn' onClick={() => detailPost(data.id)}>
                          <span>Detail</span>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            )
          }) }

          {posts.length > 0 && <div className="pagination">
            <span onClick={() => selectPageHandler(page - 1)} className={page > 1 ? "prev" : "pagination__disable"}>prev</span>

            {[...Array(posts.length / 10)].map((_, i) => {
              return <span key={i} className={page === i + 1 ? "pagination__selected" : "unselected"} onClick={() => selectPageHandler(i + 1)}>{i + 1}</span>
            })}

            <span onClick={() => selectPageHandler(page + 1)} className={page < posts.length / 10 ? "next" : "pagination__disable"}>next</span>
          </div>}
    </div>
  )
}
